Git-logger is a Bash tool to create Git Punch Cards


![git-logger-example][imgur]

Enjoy!

## How to use:

Simply copy the script ./git-logger.sh to the target git folder and type `./git-logger.sh`

## License

GPLv2+


[Imgur]:https://imgur.com/kBpTUDc.png

